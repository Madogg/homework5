/* hw5 */

	.global _start

_start:
@ Display your first and last name
	MOV R7, #4
	MOV R0, #1
	MOV R2, #10
	LDR R1, =string
	SWI 0
@ Add the numbers 2 and 7.
	MOV R3, #2
        ADD R3, R3, #7
        ADD R3, R3, #48
@ Display the result.
        LDR R4, =output
        STR R3,[R4]
        MOV R0, #1
        LDR R1, =output
        MOV R2, #5
        MOV R7, #4
        SWI 0
@ multiply the numbers 3 and 2.
	MOV R1, #3
	MOV R2, #2
        MUL R3, R1, R2
        ADD R3, R3, #48
@ Display the result.
        LDR R4, =output
        STR R3,[R4]
        MOV R0, #1
        LDR R1, =output
        MOV R2, #5
        MOV R7, #4
        SWI 0
@ Logically AND the numbers 42 and 7.
	MOV R3, #42
	AND R3, R3, #7
	ADD R3, R3, #48
@ Display the result.
	LDR R4, =output
	STR R3,[R4]
	MOV R0, #1
	LDR R1, =output
	MOV R2, #5
	MOV R7, #4
	SWI 0
@ Logically OR th enumber 5 and 2.
	MOV R3, #5
        ORR R3, R3, #2
        ADD R3, R3, #48
@ Display the result.
        LDR R4, =output
        STR R3,[R4]
        MOV R0, #1
        LDR R1, =output
        MOV R2, #5
        MOV R7, #4
        SWI 0
@ Perform the following mathmatical expression:
@ A = 24
@ B = 13
@ C = 3
@ (A-B)*C
	MOV R1, #24
	MOV R2, #13
	MOV R3, #3
	SUB R2, R1, R2
	MUL R3, R2, R3
@ Save the result of this calcilation in R0 and cleanly exit your program 
_exit:
	MOV R0, R3
        MOV R7, #1
        SWI 0

.data
string:
.ascii "Mark Dyer\n"
output:
.ascii "    \n"

